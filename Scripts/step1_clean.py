# -*- coding:utf-8 -*-
import sys
import os
reload(sys)
sys.setdefaultencoding('utf-8')

def do_clean(input_dir, output_dir):
	# read each file in input dir.
	# call replace_invalid_chars function.
	# call nomarlize function.
	# write cleaned files to output dir.

def replace_invalid_chars(input_text):
	''' Replace invalid chars.

    Invalid chars include following:
    1) chinese chars.
	2) non-standard chars.
	3)
    4)

	'''
	return input_text

def nomarlize(input_text):
	''' Nomarlize input text.
	
	'''
	return input_text



def main():
	if len(sys.args) != 3:
		return
	input_dir = sys.args[1]
	output_dir = sys.args[2]
	do_clean(input_dir, output_dir)

if __name__ == '__main__':
	main()