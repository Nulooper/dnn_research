function grad = grad_tanh_opt(z)
	grad  = (1-tanh(z).^2);
end