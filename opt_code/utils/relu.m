function X = relu(Z)
	%X = log(1 + exp(Z)); % wait to valid.
	Z(find(Z <= 0)) = 0;
	X = Z;
end