function grad = grad_sigmoid(z)
	a = sigmoid(z);
	grad = a .* (1 - a);
end