function X = sigmoid(Z)
	X = 1./(1 + exp(-Z));
end