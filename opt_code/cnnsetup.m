function cnn = cnnsetup(cnn)
    disp('-------------------Begin setting up cnn--------------------');

    % Display hyper parameters.
    disp('Hyper Parameters:');
    disp(cnn.hyper_parameters);

    % Display CNN Structure.
    disp('CNN Structure:');
    for d = 1:numel(cnn.layers)
        disp(cnn.layers{d});
    end

    % Display Free Parameters.
    disp('CNN Free Parameters:');

    cur_conv_index = 1; % index of current convolution layer.

    for l = 1 : numel(cnn.layers)   %  layer
        if strcmp(cnn.layers{l}.type, 'input')
            cnn.layers{l}.out_depth = cnn.hyper_parameters.word_embeding_dim;
            disp([num2str(l) ' ' num2str(cnn.layers{l}.out_depth)]);
        end

        if strcmp(cnn.layers{l}.type, 'sub')
            if cur_conv_index == cnn.hyper_parameters.max_conv_num
                if cnn.layers{l}.act_func ~= 0
                    cnn.layers{l}.W = normrnd(cnn.layers{l - 1}.out_depth*cnn.hyper_parameters.top_pooling_width, cnn.layers{l - 1}.feature_maps, cnn.layers{l}.feature_maps);
                    cnn.layers{l}.b = rand(cnn.layers{l - 1}.out_depth * cnn.layers{l}.feature_maps, 1);
                    
                    disp(['layer' num2str(l) '(sub)' ' weight parameters: W[' num2str(size(cnn.layers{l}.W)) ']    b[' num2str(size(cnn.layers{l}.b)) ']']);
                end
            end
            cnn.layers{l}.out_depth = cnn.layers{l - 1}.out_depth;
            disp([num2str(l) ' ' num2str(cnn.layers{l}.out_depth)]);
        end

        if strcmp(cnn.layers{l}.type, 'fold')
            if cnn.layers{l}.is_folding_on == 1
                cnn.layers{l}.out_depth = floor(cnn.layers{l - 1}.out_depth / cnn.layers{l}.fold_size);
            else
                cnn.layers{l}.out_depth = cnn.layers{l - 1}.out_depth;
            end
            disp([num2str(l) ' ' num2str(cnn.layers{l}.out_depth)]);
        end

        if strcmp(cnn.layers{l}.type, 'conv')
            cnn.layers{l}.out_depth = cnn.layers{l - 1}.out_depth;
            disp([num2str(l) ' ' num2str(cnn.layers{l}.out_depth)]);
            cnn.layers{l}.W = normrnd(0,0.05,[cnn.layers{l - 1}.out_depth*cnn.layers{l}.kernel_col, cnn.layers{l-1}.feature_maps, cnn.layers{l}.feature_maps]);
            cnn.layers{l}.b = rand(cnn.layers{l - 1}.out_depth, cnn.layers{l}.feature_maps);
            cur_conv_index = cur_conv_index + 1;

            disp(['layer' num2str(l) '(conv)' ' weight parameters: W[' num2str(size(cnn.layers{l}.W)) ']    b[' num2str(size(cnn.layers{l}.b)) ']']);
        end
    end

    % Initialize weights of the first full connection layer.
    cnn.layers{l + 1}.W = normrnd(0, 0.05, [cnn.hyper_parameters.sent_embeding_dim, cnn.layers{l}.out_depth*cnn.hyper_parameters.top_pooling_width*cnn.layers{l}.feature_maps]);
    cnn.layers{l + 1}.b = rand(cnn.hyper_parameters.sent_embeding_dim, 1);
    cnn.layers{l + 1}.type = 'full';

    disp(['layer' num2str(l+1) '(full)' ' weight parameters: W[' num2str(size(cnn.layers{l+1}.W)) ']    b[' num2str(size(cnn.layers{l+1}.b)) ']']);

    % Initialize weights of prediction hidden layer.
    cnn.layers{l + 2}.W = normrnd(0, 0.05, [cnn.hyper_parameters.predict_hidden_num, cnn.hyper_parameters.sent_embeding_dim * cnn.hyper_parameters.windows_size]);
    cnn.layers{l + 2}.b = rand(cnn.hyper_parameters.predict_hidden_num, 1);
    disp(['layer' num2str(l+2) '(full)' ' weight parameters: W[' num2str(size(cnn.layers{l+2}.W)) ']    b[' num2str(size(cnn.layers{l+2}.b)) ']']);

    % Initialize weights of prediction output layer.
    cnn.layers{l + 3}.W = normrnd(0, 0.05, [cnn.hyper_parameters.sent_embeding_dim, cnn.hyper_parameters.predict_hidden_num]);
    cnn.layers{l + 3}.b = rand(cnn.hyper_parameters.sent_embeding_dim, 1);
    disp(['layer' num2str(l+3) '(full)' ' weight parameters: W[' num2str(size(cnn.layers{l+3}.W)) ']    b[' num2str(size(cnn.layers{l+3}.b)) ']']);

    disp('-------------------End setting up cnn--------------------');
end
