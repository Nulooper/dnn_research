function folded_result = folding(X, fold_size)
	[row col] = size(X);

	int_fold = floor(row / fold_size);
	if int_fold ~= (row / fold_size)
		X = X(1:int_fold * fold_size, :);
	end

	[row col] = size(X);

	b = reshape(X, fold_size, floor(row * col / fold_size));
	c = sum(b, 1);
	folded_result = reshape(c, int_fold, col);
end