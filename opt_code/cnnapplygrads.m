function cnn = cnnapplygrads(cnn, grad)
	n = numel(cnn.layers);
	%disp(['size: ' numel(grad)]);
	for l = 2 : n - 3
		if strcmp(cnn.layers{l}.type, 'conv')
            cnn.layers{l}.W = cnn.layers{l}.W - cnn.hyper_parameters.learning_rate * grad{l}.W;
            cnn.layers{l}.b = cnn.layers{l}.b - cnn.hyper_parameters.learning_rate * grad{l}.b;
		end
	end 

	% Apply grad to full connection layers.
	cnn.layers{n - 2}.W = cnn.layers{n - 2}.W - cnn.hyper_parameters.learning_rate * grad{n -2}.W;
	cnn.layers{n - 2}.b = cnn.layers{n - 2}.b - cnn.hyper_parameters.learning_rate * grad{n - 2}.b;

	cnn.layers{n - 1}.W = cnn.layers{n - 1}.W - cnn.hyper_parameters.learning_rate * grad{n - 1}.W;
	cnn.layers{n - 1}.b = cnn.layers{n - 1}.b - cnn.hyper_parameters.learning_rate * grad{n - 1}.b;

	cnn.layers{n}.W = cnn.layers{n}.W - cnn.hyper_parameters.learning_rate * grad{n}.W;
	cnn.layers{n}.b = cnn.layers{n}.b - cnn.hyper_parameters.learning_rate * grad{n}.b;
end