function [result, k_indices] = k_max_pooling(X, k)
	[row,col] = size(X);

	if k <= 1
		[v, ix] = max(X');
		result = v';
		k_indices = ix';
	else
		result = zeros(row, k);
		k_indices = zeros(row, k);

		copy_x = X;
		for i = 1:row
			for j = 1:k
				[v, ix] = max(copy_x(i, :));
				copy_x(i, ix) = -Inf;
				k_indices(i, j) = ix;
			end
			k_indices(i, 1:end) = sort(k_indices(i, 1:end));
			result(i,1:end) = X(i, k_indices(i, 1:end));
		end
	end
end

