function [cnn, grad] = dcnn_bp(cnn, context_sents_vec, target_sent_vec)

	%%%%
	% Compute error term of last layer.
	n = numel(cnn.layers);

	cnn.layers{n}.out_diff = cnn.layers{n}.a{1} - target_sent_vec;
	cnn.layers{n}.error = cnn.layers{n}.out_diff.*(cnn.layers{n}.a{1}.*(1 - cnn.layers{n}.a{1})); % Default sigmoid as non-linearity operation.
	grad{n}.W =  cnn.layers{n}.error * (cnn.layers{n - 1}.a{1})' + cnn.hyper_parameters.lambda.*cnn.layers{n}.W;
	grad{n}.b =  cnn.layers{n}.error;


	cnn.layers{n - 1}.out_diff = (cnn.layers{n}.W)' * cnn.layers{n}.error;
	cnn.layers{n - 1}.error = cnn.layers{n - 1}.out_diff.*cnn.layers{n - 1}.a{1}.*(1 - cnn.layers{n - 1}.a{1});
    grad{n - 1}.W = cnn.layers{n - 1}.error * context_sents_vec' + cnn.hyper_parameters.lambda.*cnn.layers{n - 1}.W;
	grad{n - 1}.b = cnn.layers{n - 1}.error;

    %%%%%%%%%%%%%%%%%%
	cnn.layers{n - 2}.out_diff = (cnn.layers{n - 1}.W)' * cnn.layers{n - 1}.error;
	cnn.layers{n - 2}.error = cnn.layers{n - 2}.out_diff.*context_sents_vec.*(1 - context_sents_vec);
    tmp_error = reshape(cnn.layers{n - 2}.error, cnn.hyper_parameters.sent_embeding_dim, cnn.hyper_parameters.windows_size);
    cnn.layers{n - 2}.error = tmp_error(:, end);
	%cnn.layers{n - 2}.error = sum(reshape(cnn.layers{n - 2}.error, cnn.hyper_parameters.sent_embeding_dim, cnn.hyper_parameters.windows_size), 2) / cnn.hyper_parameters.windows_size;
	grad{n - 2}.W = reshape(cnn.layers{n - 2}.error * (cnn.last_sub_vec)', cnn.hyper_parameters.sent_embeding_dim, cnn.layers{n-3}.out_depth*cnn.hyper_parameters.top_pooling_width*cnn.layers{n - 3}.feature_maps);
	grad{n - 2}.b = cnn.layers{n - 2}.error;
    %%%%%%%%%%%%%%%%%%

	% Iterate to get error term.
	for i = (n - 3) : -1 : 2
        
		if strcmp(cnn.layers{i}.type, 'conv')
			%%%%%%%%%%%%%%Compute error term.
			if strcmp(cnn.layers{i + 1}.type, 'fold') & (cnn.layers{i + 1}.is_folding_on == 1)
				for j = 1:cnn.layers{i}.feature_maps
					% Compute error term
					cnn.layers{i}.error(:,:,j) = kron(cnn.layers{i + 1}.error(:,:,j), ones(cnn.layers{i+1}.fold_size, 1))./cnn.layers{i+1}.fold_size;
                    
					cnn.layers{i}.error(:,:,j) = cnn.layers{i}.error(:,:,j).*check_grad(cnn.layers{i}.a{j}, cnn.layers{i}.act_func);
					
				end
				cnn.hyper_parameters.word_embeding_dim = cnn.hyper_parameters.word_embeding_dim * cnn.layers{i + 1}.fold_size;

			elseif strcmp(cnn.layers{i + 2}.type, 'sub')
                
				[row, col] = size(cnn.layers{i}.a{1});
        		cnn.layers{i}.error = zeros(row, col, cnn.layers{i}.feature_maps);
				for j = 1:cnn.layers{i}.feature_maps
        			%k_indices = cnn.layers{i + 1}.a{j}.k_indices;
                    k_indices = cnn.layers{i + 2}.k_indices{j};


        			%k_col = size(k_indices, 2); % column of matrix k_indices.
        			%row_index = kron(1:row, ones(1, k_col));
        			%col_index = reshape(k_indices', 1, row * k_col);
        			%v_value = reshape((cnn.layers{i + 1}.error(:,:,j))', 1, row * k_col);
        			%cnn.layers{i}.error(:,:,j)(sub2ind([row, col], row_index, col_index)) = v_value;
        			
        			
        			for m = 1:row
        				for x = 1:size(k_indices, 2)
        				    cnn.layers{i}.error(m, k_indices(m, x), j) = cnn.layers{i + 2}.error(m, x, j);
        				end
        			end
        			
        			cnn.layers{i}.error(:,:,j) = cnn.layers{i}.error(:,:,j).*check_grad(cnn.layers{i}.a{j}, cnn.layers{i}.act_func);
        		end
			end

			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			% Compute grad.
            grad{i}.W = zeros(cnn.layers{i-1}.out_depth*cnn.layers{i}.kernel_col, cnn.layers{i - 1}.feature_maps, cnn.layers{i}.feature_maps);
			for j = 1:cnn.layers{i}.feature_maps
				for m = 1:cnn.layers{i - 1}.feature_maps
                    grad{i}.W(:, m, j) = reshape(fliplr(fastConv(cnn.layers{i}.error(:,:,j), fliplr(cnn.layers{i - 1}.a{m}), 'v', cnn.hyper_parameters.is_gpu_on)), cnn.layers{i-1}.out_depth*cnn.layers{i}.kernel_col, 1);
				end
				grad{i}.b(:, j) = sum(cnn.layers{i}.error(:,:,j), 2);
			end

        elseif strcmp(cnn.layers{i}.type, 'fold')
        	if cnn.layers{i}.is_folding_on == 1
        		[row, col] = size(cnn.layers{i}.a{1});
        		cnn.layers{i}.error = zeros(row, col, cnn.layers{i}.feature_maps);
        		for j = 1:cnn.layers{i}.feature_maps
                    k_indices = cnn.layers{i + 1}.k_indices{j};
        			for m = 1:row
        				for x = 1:size(k_indices, 2)
        				    cnn.layers{i}.error(m, k_indices(m, x), j) = cnn.layers{i + 1}.error(m, x, j) * cnn.layers{i}.fold_size; %%%%%%
        				end
        			end
        		end
        	end
        			

        elseif strcmp(cnn.layers{i}.type, 'sub')
        	%%%%%%%%%%%%%%%%%%%
        	% Compute error term.
        	if strcmp(cnn.layers{i + 1}.type, 'full')
        		% Compute error term.
            	cnn.layers{i}.out_diff = (cnn.layers{i + 1}.W)' * cnn.layers{i + 1}.error;
            	cnn.layers{i}.error = reshape(cnn.layers{i}.out_diff, cnn.layers{i}.out_depth, cnn.hyper_parameters.top_pooling_width, cnn.layers{i}.feature_maps);

            elseif strcmp(cnn.layers{i + 1}.type, 'conv')
                [row, col] = size(cnn.layers{i}.a{1});
                cnn.layers{i}.error = zeros(row, col, cnn.layers{i}.feature_maps);
                disp(size(cnn.layers{i}.error));
            	for j = 1:cnn.layers{i}.feature_maps
            		for k = 1:cnn.layers{i + 1}.feature_maps
            			tmp_kernal = cnn.layers{i + 1}.W(:, j, k);
                        %disp(size(cnn.layers{i + 1}.error(:,:,k)));
                        %disp(size(fliplr(reshape(tmp_kernal, cnn.hyper_parameters.word_embeding_dim, cnn.layers{i+1}.kernel_col))));
            			cnn.layers{i}.error(:,:,j) = cnn.layers{i}.error(:,:,j) + fastConv(cnn.layers{i + 1}.error(:,:,k), reshape(tmp_kernal, cnn.layers{i+1}.out_depth, cnn.layers{i+1}.kernel_col), 'v', cnn.hyper_parameters.is_gpu_on);
            		end
            	end
            end

        end
	end
end