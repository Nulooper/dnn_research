function [cnn, cost] = dcnn_ff2(cnn, context_sents_vec, target_sent_vec)
	%
	% context_sents_vec: [cnn.hyper_parameters.sent_embeding_dim * cnn.hyper_parameters.window_size, 1]
	% target_sent_vec: [cnn.hyper_parameters.sent_embeding_dim, 1]
	%

	% forward output
	n = numel(cnn.layers);
	cnn.layers{n - 1}.a{1} = sigmoid(cnn.layers{n - 1}.W * context_sents_vec + cnn.layers{n - 1}.b);
	cnn.layers{n}.a{1} = sigmoid(cnn.layers{n}.W * cnn.layers{n - 1}.a{1} + cnn.layers{n}.b);

	%disp(num2str(cnn.layers{n}.a{1}));

	% compute cost.
	cost = (1 / 2) * sum((cnn.layers{n}.a{1} - target_sent_vec).^2);
	% add L2 regularization penalty term.
	tmp1 = (cnn.layers{n}.W).^2;
	tmp2 = (cnn.layers{n - 1}.W).^2;
	cost = cost + cnn.hyper_parameters.lambda * (sum(tmp1(:)) + sum(tmp2(:))) / 2;
end