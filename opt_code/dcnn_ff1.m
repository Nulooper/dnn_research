function [cnn, conv_sent_vec] = dcnn_ff1(cnn, sentence, sent_length, is_gpu_on)
	%
	% sentence: [cnn.hyper_parameters.sent_embeding_dim, cnn.hyper_parameters.max_sent_length] 
	% sent_length: the number of words in input sentence. 
	%
	%%%%
	sentence = sentence(:, 1:sent_length);
    %disp('sentence');
    %disp(sentence);
	cur_conv_index = 1;

	n = numel(cnn.layers);
    cnn.layers{1}.a{1} = sentence;

    for l = 2 : n - 3   %  for each layer
        if strcmp(cnn.layers{l}.type, 'conv')

        	for j = 1 : cnn.layers{l}.feature_maps   %  for each feature map
                %  create temp output map
                z = zeros(size(cnn.layers{l-1}.a{1}, 1), size(cnn.layers{l-1}.a{1}, 2) + cnn.layers{l}.kernel_col - 1);
                for i = 1 : cnn.layers{l-1}.feature_maps  %  for each input map
                    %  convolve with corresponding kernel and add to temp output map
                    z = z + fastConv(cnn.layers{l - 1}.a{i}, fliplr(reshape(cnn.layers{l}.W(:, i, j), cnn.layers{l}.out_depth, cnn.layers{l}.kernel_col)), 'f', is_gpu_on);
                end
                % add bias
                z = z + repmat(cnn.layers{l}.b(:, j), 1, size(z, 2));
                % nonlinearity operation.
                cnn.layers{l}.a{j} = check_act_func(z, cnn.layers{l}.act_func);
            end
            cur_conv_index = cur_conv_index + 1;

        elseif strcmp(cnn.layers{l}.type, 'fold')
        	%%%%%%;
        	if cnn.layers{l}.is_folding_on == 1
        	    for f = 1:cnn.layers{l}.feature_maps
        		    cnn.layers{l}.a{f} = folding(cnn.layers{l -1}.a{f}, cnn.layers{l}.fold_size);
        	    end
        	end
        

        elseif strcmp(cnn.layers{l}.type, 'sub')
            %  compute the k value of current convolution layer.
            d_k = compute_dynamic_k(cnn, cur_conv_index, sent_length);
            for j = 1 : cnn.layers{l}.feature_maps
                if strcmp(cnn.layers{l - 1}.type, 'conv')
            	    %[cnn.layers{l}.a{j}, cnn.layers{l}.a{j}.k_indices] = k_max_pooling(cnn.layers{l-2}.a{j}, cnn.layers{l}.pool_size);
                    [cnn.layers{l}.a{j}, cnn.layers{l}.k_indices{j}] = k_max_pooling(cnn.layers{l-2}.a{j}, cnn.layers{l}.pool_size);
                elseif strcmp(cnn.layers{l - 1}.type, 'fold') & (cnn.layers{l - 1}.is_folding_on == 0)
                    %[cnn.layers{l}.a{j}, cnn.layers{l}.a{j}.k_indices] = k_max_pooling(cnn.layers{l-2}.a{j}, cnn.layers{l}.pool_size);
                    [cnn.layers{l}.a{j}, cnn.layers{l}.k_indices{j}] = k_max_pooling(cnn.layers{l-2}.a{j}, cnn.layers{l}.pool_size);
                else
                    %[cnn.layers{l}.a{j}, cnn.layers{l}.a{j}.k_indices] = k_max_pooling(cnn.layers{l-1}.a{j}, cnn.layers{l}.pool_size);
                    [cnn.layers{l}.a{j}, cnn.layers{l}.k_indices{j}] = k_max_pooling(cnn.layers{l-1}.a{j}, cnn.layers{l}.pool_size);
                end
            end
        end
    end

    % 将最后一个采样曾reshape为一个向�? 向量的行数目可以理解为最后一个层采样层被拉平后的神经元的数目.
    cnn.last_sub_vec = [];
    for j = 1 : numel(cnn.layers{l}.a)
        [row col] = size(cnn.layers{l}.a{j});
        cnn.last_sub_vec = [cnn.last_sub_vec; reshape(cnn.layers{l}.a{j}, row*col, 1)];
    end

    %  feedforward into output perceptrons
    cnn.layers{n - 2}.a{1} = sigmoid(cnn.layers{n - 2}.W * cnn.last_sub_vec + cnn.layers{n - 2}.b);

    % Assign return value.
    conv_sent_vec = cnn.layers{n - 2}.a{1};
end