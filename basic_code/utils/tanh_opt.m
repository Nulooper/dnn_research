function f = tanh_opt(A)
	f = 1.7158 * tanh(2 / 3.*A);
end