function result = check_grad(X, act_func)
	if act_func == 0
        result = X;
    elseif act_func == 1
        result = grad_relu(X);
    elseif act_func == 2
        result = grad_sigmoid(X);
    elseif act_func == 3
        result = grad_tanh_opt(X);
    end	
end