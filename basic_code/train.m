function cnn = train(cnn, sent_matrix, sent_len_vec, fragment_index_vec)
	
	batch_num = size(fragment_index_vec, 1); % total batch number of given train dataset.
    disp(['number of batch: ' num2str(batch_num)]); 

	for i = 1:cnn.hyper_parameters.max_epochs
		statistics{i}.cost = []; % for error analysis.
		disp(['epoch ' num2str(i) '/' num2str(cnn.hyper_parameters.max_epochs)]);

    	last_index = 0;
    	for j = 1 : batch_num
            disp(['last_index: ' num2str(last_index + 1) ', fragment: ' num2str(fragment_index_vec(j))]);
        	mini_batch = sent_matrix(:, :, (last_index + 1) : fragment_index_vec(j));
       	 	mini_batch_len_vec = sent_len_vec(last_index + 1 : fragment_index_vec(j));
        	%%%%
        	batch_size = size(mini_batch, 3);
        	context_sents_vec = zeros(cnn.hyper_parameters.sent_embeding_dim, cnn.hyper_parameters.windows_size + 1);

        	flag = 0;
            sum_grad{1}.W = zeros(0);
            sum_grad{1}.b = zeros(0);


            %%%%

            % Copy window_size to adapt the current epoch.
            cur_windows_size = cnn.hyper_parameters.windows_size;

            % if batch_size equals one, then ignore current batch.
            disp(['batch_size: ' num2str(batch_size)]);
            if batch_size == 1 
                continue
            end
            if batch_size == 0
                continue
            end
            disp(['not continue: ' num2str(batch_size)]);
            % Check if batch_size is less than window_size, then make window_size equal batch_size minus one.
            if batch_size < cur_windows_size
                cur_windows_size = batch_size - 1;
            end

            %%%%

        	avg = 1 / (batch_size - cnn.hyper_parameters.windows_size);
        	stat_avg_cost = 0; 

        	for n = 1 : (batch_size - cur_windows_size)
        		for m = 0:cur_windows_size
        			%context_vec(:, n + m) = dcnn_ff1(cnn, mini_batch(:,:,n+m), mini_batch_len_vec(n+m), cnn.hyper_parameters.is_gpu_on);
                    [cnn, context_sents_vec(:, m + 1)] = dcnn_ff1(cnn, mini_batch(:,:,n+m), mini_batch_len_vec(n+m), cnn.hyper_parameters.is_gpu_on);
        		end
                disp(size(context_sents_vec));

                [cnn, cost] = dcnn_ff2(cnn, reshape(context_sents_vec(:, 1:cur_windows_size), cnn.hyper_parameters.sent_embeding_dim*cur_windows_size, 1), context_sents_vec(:,cur_windows_size+1));
                stat_avg_cost = stat_avg_cost + cost * avg;

                %disp('Last activiation: ');
                %disp(cnn.layers{10}.a{1});
                disp(['cost: ' num2str(cost) '!']);

                target_sent_vec = context_sents_vec(:, cur_windows_size + 1);
                [cnn, grad] = dcnn_bp(cnn, reshape(context_sents_vec(:, 1:cur_windows_size), cnn.hyper_parameters.sent_embeding_dim*cur_windows_size, 1), target_sent_vec);

                if flag == 0
                	% Initialize and accumulate the current grad.
                	for g = 1:numel(grad)
                        if sum(size(grad{g})) ~= 0 %%% Check g th cell is initialized.
                            sum_grad{g}.W = zeros(size(grad{g}.W));
                		    sum_grad{g}.W = sum_grad{g}.W + grad{g}.W * avg;

                		    sum_grad{g}.b = zeros(size(grad{g}.b));
                		    sum_grad{g}.b = sum_grad{g}.b + grad{g}.b * avg;
                        end
                	end
                	flag = 1;
                else
                	% Accumulate grad.
                	for g = 1:numel(grad)
                        if sum(size(grad{g})) ~= 0
                		    sum_grad{g}.W = sum_grad{g}.W + grad{g}.W * avg;
                		    sum_grad{g}.b = sum_grad{g}.b + grad{g}.b * avg;
                        end
                	end
                end
       	 	end
       	 	disp(['epoch  ' num2str(i)  ' batch ' num2str(j)  ':  stat_avg_cost: ' num2str(stat_avg_cost)]);

       	 	statistics{i}.cost = [statistics{i}.cost; stat_avg_cost];
       	 	cnn = cnnapplygrads(cnn, sum_grad);

        	%%%%
        	last_index = fragment_index_vec(j);
    	end

    	statistics{i}.avg = mean(statistics{i}.cost);
    	disp(['epoch ' num2str(i) ' average cost: ' num2str(statistics{i}.avg)]);
	end

	% Plot cost figure. x-axis: epoch; y-axis: average cost of each epoch.
	x = 1:cnn.hyper_parameters.max_epochs;
	y = [];
	for i = 1:cnn.hyper_parameters.max_epochs
		y = [y; statistics{i}.avg]
	end
	plot(x, y);


end