function checkDCNNGradients()

	cnn.hyper_parameters.word_embeding_dim = 8; % word embeding dimension. It must be equals 2.^n.
	cnn.hyper_parameters.sent_embeding_dim = 8; % sentence embeding dimension.
	cnn.hyper_parameters.learning_rate = 0.025; % learning rate.
	cnn.hyper_parameters.dropout_threshold = 0.2; % full connected layer dropout threshold.
	cnn.hyper_parameters.max_sent_length = 58; % maximum sentence length.
	cnn.hyper_parameters.max_epochs = 10; % number of epochs.
	cnn.hyper_parameters.is_gpu_on = 0; % if use GPU.
	cnn.hyper_parameters.windows_size = 3; % windows size.
	cnn.hyper_parameters.lambda = 0.02; % the weight of regularization term.
	cnn.hyper_parameters.top_pooling_width = 2; % the width of top pooling.
	cnn.hyper_parameters.predict_hidden_num = 5; % prediction full connected layer
	cnn.hyper_parameters.max_conv_num = 1; % the max number of convolution layers.
    cnn.hyper_parameters.dropoutFraction = 0.5;
    cnn.hyper_parameters.training = 1;

	epsilon = 1e-4;
	depth_epsilon = kron(epsilon, ones(cnn.hyper_parameters.word_embeding_dim, 1));
    er      = 1e-8;

	cnn.layers = {
    struct('type', 'input', 'feature_maps', 1) %input layer

    struct('type', 'conv', 'feature_maps', 6, 'kernel_row', 1, 'kernel_col', 2, 'act_func', 1)
    struct('type', 'fold', 'feature_maps', 6, 'fold_size', 2, 'is_folding_on', 1) 
    struct('type', 'sub', 'feature_maps', 6, 'is_kmax', 1, 'pool_size', 2, 'act_func', 0) 
    };

    cnn = cnnsetup(cnn);
    n = numel(cnn.layers);

    sent_matrix = randn(8, 4, 4); 
    sent_len_vec = ones(4,1)*4;

    cnn_sent_vec = zeros(8, 4);
    for s = 1:2
    	[cnn, cnn_sent_vec(:, s)] = dcnn_ff1(cnn, sent_matrix(:, :, s), sent_len_vec(s,1), 0);
    end
    [cnn, cnn_sent_vec(:, 4)] = dcnn_ff1(cnn, sent_matrix(:, :, 4), sent_len_vec(4,1), 0);
    [cnn, cnn_sent_vec(:, 3)] = dcnn_ff1(cnn, sent_matrix(:, :, 3), sent_len_vec(3,1), 0);

    [cnn, cnn_cost] = dcnn_ff2(cnn, reshape(cnn_sent_vec(:, 1:3), 8*3, 1), cnn_sent_vec(:, 4));
    [cnn, cnn_grad] = dcnn_bp(cnn, reshape(cnn_sent_vec(:, 1:3), 8*3, 1), cnn_sent_vec(:, 4));

    % Check graddients of full connection layers.
    for s = 0 : 1
    	% check bias.
    	disp('Checking bias of full connection layers.');
    	for j = 1 : numel(cnn.layers{n - s}.b)
    		cnn_m = cnn;
    		cnn_p = cnn;
    		cnn_m.layers{n - s}.b(j) = cnn_m.layers{n - s}.b(j) - epsilon;
    		cnn_p.layers{n - s}.b(j) = cnn_p.layers{n - s}.b(j) + epsilon;

    		cnn_m_sent_vec = zeros(8, 4);
    		cnn_p_sent_vec = zeros(8, 4);
    		for k = 1:4
    			[cnn_m, cnn_m_sent_vec(:, k)] = dcnn_ff1(cnn_m, sent_matrix(:, :, k), sent_len_vec(k,1), 0);
    			[cnn_p, cnn_p_sent_vec(:, k)] = dcnn_ff1(cnn_p, sent_matrix(:, :, k), sent_len_vec(k,1), 0);
    		end

    		[cnn_m, cnn_m_cost] = dcnn_ff2(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
    		[cnn_p, cnn_p_cost] = dcnn_ff2(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

    		[cnn_m, cnn_m_grad] = dcnn_bp(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
    		[cnn_p, cnn_p_grad] = dcnn_bp(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

    		d = (cnn_p_cost - cnn_m_cost) / (2 * epsilon);
    		e = abs(d - cnn_grad{n -s}.b(j));
    		disp([n-s j d cnn_grad{n -s}.b(j)]);
    		if e > er
    			error('numerical gradient checking failed.');
    		end
    	end

    	% check weights.
    	disp('Checking weights of full connection layers.');
    	for i = 1 : size(cnn.layers{n - s}.W, 1)
    		for u = 1 : size(cnn.layers{n - s}.W, 2)
    			cnn_m = cnn;
    			cnn_p = cnn;

    			cnn_p.layers{n - s}.W(i, u) = cnn_p.layers{n - s}.W(i, u) + epsilon;
    			cnn_m.layers{n - s}.W(i, u) = cnn_m.layers{n - s}.W(i, u) - epsilon;

    			cnn_m_sent_vec = zeros(8, 4);
    			cnn_p_sent_vec = zeros(8, 4);
    			for k = 1:4
    				[cnn_m, cnn_m_sent_vec(:, k)] = dcnn_ff1(cnn_m, sent_matrix(:, :, k), sent_len_vec(k,1), 0);
    				[cnn_p, cnn_p_sent_vec(:, k)] = dcnn_ff1(cnn_p, sent_matrix(:, :, k), sent_len_vec(k,1), 0);
    			end

    			[cnn_m, cnn_m_cost] = dcnn_ff2(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
    			[cnn_p, cnn_p_cost] = dcnn_ff2(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

    			[cnn_m, cnn_m_grad] = dcnn_bp(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
    			[cnn_p, cnn_p_grad] = dcnn_bp(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

    			d = (cnn_p_cost - cnn_m_cost) / (2 * epsilon);
    			e = abs(d - cnn_grad{n -s}.W(i, u));
    			if e > er
    				error('numerical gradient checking failed.');
    			end
    		end
    	end
    end

    % Check gradients for convolution layers, as in our design, only convolution layers have weights and bias. 
    for l = n - 3 : -1 : 2
    	if strcmp(cnn.layers{l}.type, 'conv')
    		for j = 1 : numel(cnn.layers{l}.a)
                for q = 1:size(cnn.layers{l}.b, 1)
                    cnn_m = cnn; cnn_p = cnn;
                    cnn_p.layers{l}.b(q, j) = cnn_p.layers{l}.b(q, j) + epsilon;
                    cnn_m.layers{l}.b(q, j) = cnn_m.layers{l}.b(q, j) - epsilon;

                    cnn_m_sent_vec = zeros(8, 4);
                    cnn_p_sent_vec = zeros(8, 4);

                    for k = 1:2
                        cnn_m_sent_vec(:,k) = cnn_sent_vec(:, k);
                        cnn_p_sent_vec(:,k) = cnn_sent_vec(:, k);
                    end

                    [cnn_m, cnn_m_sent_vec(:, 3)] = dcnn_ff1(cnn_m, sent_matrix(:, :, 3), sent_len_vec(3, 1), 0);
                    [cnn_p, cnn_p_sent_vec(:, 3)] = dcnn_ff1(cnn_p, sent_matrix(:, :, 3), sent_len_vec(3, 1), 0);

                    cnn_m_sent_vec(:,4) = cnn_sent_vec(:, 4);
                    cnn_p_sent_vec(:,4) = cnn_sent_vec(:, 4);


                    [cnn_m, cnn_m_cost] = dcnn_ff2(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
                    [cnn_p, cnn_p_cost] = dcnn_ff2(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

                    [cnn_m, cnn_m_grad] = dcnn_bp(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
                    [cnn_p, cnn_p_grad] = dcnn_bp(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

                    d = (cnn_p_cost - cnn_m_cost) / (2 * epsilon);
                    e = abs(d - cnn_grad{l}.b(q, j));
                    disp(['d: ' num2str(d)]);
                    disp(['cnn_grad{l}.b(q, j):' num2str(cnn_grad{l}.b(q, j))]);
                    disp(['e:' num2str(e)]);
                    if e > er
                        error('numerical gradient checking failed.');
                    end
                

                    for i = 1 : numel(cnn.layers{l - 1}.a)
                        for u = 1 : cnn.layers{l - 1}.out_depth
                            for v = 1 : cnn.layers{l}.kernel_col
                                cnn_m = cnn; cnn_p = cnn;
                            
                                tmp_m_weights = cnn_m.layers{l}.W(:, i, j);
                                tmp_m_weights = reshape(tmp_m_weights, cnn_m.layers{l}.out_depth, cnn_m.layers{l}.kernel_col);
                                tmp_m_weights(u, v) = tmp_m_weights(u ,v) - epsilon;
                                cnn_m.layers{l}.W(:, i, j) = reshape(tmp_m_weights, cnn_m.layers{l}.out_depth * cnn_m.layers{l}.kernel_col, 1);

                                tmp_p_weights = cnn_p.layers{l}.W(:, i, j);
                                tmp_p_weights = reshape(tmp_p_weights, cnn_p.layers{l}.out_depth, cnn_p.layers{l}.kernel_col);
                                tmp_p_weights(u, v) = tmp_p_weights(u ,v) + epsilon;
                                cnn_p.layers{l}.W(:, i, j) = reshape(tmp_p_weights, cnn_p.layers{l}.out_depth * cnn_p.layers{l}.kernel_col, 1);

                                cnn_m_sent_vec = zeros(8, 4);
                                cnn_p_sent_vec = zeros(8, 4);
                                for k = 1:2
                                    cnn_m_sent_vec(:, k) = cnn_sent_vec(:, k);
                                    cnn_p_sent_vec(:, k) = cnn_sent_vec(:, k);
                                end

                                [cnn_m, cnn_m_sent_vec(:, 3)] = dcnn_ff1(cnn_m, sent_matrix(:, :, 3), sent_len_vec(3, 1), 0);
                                [cnn_p, cnn_p_sent_vec(:, 3)] = dcnn_ff1(cnn_p, sent_matrix(:, :, 3), sent_len_vec(3, 1), 0);

                                cnn_m_sent_vec(:, 4) = cnn_sent_vec(:, 4);
                                cnn_p_sent_vec(:, 4) = cnn_sent_vec(:, 4);

                                [cnn_m, cnn_m_cost] = dcnn_ff2(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
                                [cnn_p, cnn_p_cost] = dcnn_ff2(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

                                [cnn_m, cnn_m_grad] = dcnn_bp(cnn_m, reshape(cnn_m_sent_vec(:, 1:3), 8*3, 1), cnn_m_sent_vec(:, 4));
                                [cnn_p, cnn_p_grad] = dcnn_bp(cnn_p, reshape(cnn_p_sent_vec(:, 1:3), 8*3, 1), cnn_p_sent_vec(:, 4));

                                d = (cnn_p_cost - cnn_m_cost) ./ (2 * epsilon);
                                %d = (cnn_p_cost - cnn_m_cost) ./ (2 * epsilon);
                                disp('d');
                                disp(d);
                                %e = norm(d - cnn_grad{l}.W(1, i, c)) / norm(d + cnn_grad{l}.W(1, i, j));
                                tmp_W = reshape(cnn_grad{l}.W(:, i, j), cnn.layers{l}.out_depth, cnn.layers{l}.kernel_col);
                                e = abs(d - tmp_W(u, v));
                                disp('W');
                                disp(tmp_W(u, v));
                                %e = abs(d - cnn_grad{l}.W(1, i, c));
                                disp('e');
                                disp(e);
                                if e > er
                                    error('numerical gradient checking failed.');
                                end
                            end
                        end
                    end
                %%%%%%%%%%%%%%%%%%%%%    
                end
    			
    		end
    	end
    end
end