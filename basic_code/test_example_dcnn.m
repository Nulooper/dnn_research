function test_example_dcnn


addpath(genpath('./utils'))

%sent_matrix = load('sent_matrix.mat'); % variable 'sent_matrix' represents d*max_sent_length*samples_num matrix.
%sent_len_vec = load('sent_len_vec.mat'); % variable 'sent_len_vec' represents samples_num*1 vector.
%fragment_index_vec = load('fragment_index_vec.mat'); % last index of text fragment.
load('../data/train_para_matrix.mat');
load('../data/train_para_len_vec.mat');
load('../data/train_para_index_vec.mat');
%load vocab_emb_matrix; % word embeding matrix.

%% ex1 Train a 6c-2s-12c-2s Convolutional neural network 
%will run 1 epoch in about 200 second and get around 11% error. 
%With 100 epochs you'll get around 1.2% error

cnn.hyper_parameters.word_embeding_dim = 50; % word embeding dimension.
cnn.hyper_parameters.sent_embeding_dim = 30; % sentence embeding dimension.
cnn.hyper_parameters.learning_rate = 0.025; % learning rate.
cnn.hyper_parameters.dropout_threshold = 0.2; % full connected layer dropout threshold.
cnn.hyper_parameters.max_sent_length = 100; % maximum sentence length.
cnn.hyper_parameters.max_epochs = 10; % number of epochs.
cnn.hyper_parameters.is_gpu_on = 0; % if use GPU.
cnn.hyper_parameters.windows_size = 2; % windows size.
cnn.hyper_parameters.lambda = 0.02; % the weight of regularization term.
cnn.hyper_parameters.top_pooling_width = 2; % the width of top pooling.
cnn.hyper_parameters.predict_hidden_num = 200; % prediction full connected layer
cnn.hyper_parameters.max_conv_num = 2; % the max number of convolution layers.



cnn.layers = {
    struct('type', 'input', 'feature_maps', 1) %input layer

    % act_func: 0 for not using activation function, 1 for relu, 2 for sigmoid, 3 for tanh_opt
    struct('type', 'conv', 'feature_maps', 6, 'kernel_row', 1, 'kernel_col', 2, 'act_func', 1) % convolution layer
    struct('type', 'fold', 'feature_maps', 6, 'fold_size', 2, 'is_folding_on', 0) % folding layer
    struct('type', 'sub', 'feature_maps', 6, 'is_kmax', 1, 'pool_size', 2, 'act_func', 0) % sub sampling layer
                                                                                          % if 'is_kmax' equals 0
                                                                                          % then use pool_size.
                                                                                          % else, use k_max_pooling.

    % act_func: 0 for not using activation function, 1 for relu, 2 for sigmoid, 3 for tanh_opt
    struct('type', 'conv', 'feature_maps', 12, 'kernel_row', 1, 'kernel_col', 2, 'act_func', 1) % convolution layer
    struct('type', 'fold', 'feature_maps', 12, 'fold_size', 2, 'is_folding_on', 0) % folding layer
    struct('type', 'sub', 'feature_maps', 12, 'is_kmax', 1, 'pool_size', 2, 'act_func', 0) % sub sampling layer
                                                                                           % if 'is_kmax' equals 0
                                                                                           % Then use pool_size.
                                                                                           % else, use k_max_pooling.
};

% Initialize initial parameters for this CNN.
cnn = cnnsetup(cnn);
%cnn.word_embeding = word_embeding_matrix;


% Train the CNN.
cnn = train(cnn, train_para_matrix, train_para_len_vec, train_para_index_vec);


save cnn;
