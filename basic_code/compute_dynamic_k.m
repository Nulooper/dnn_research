function k = compute_dynamic_k(cnn, cur_conv_index, cur_sent_length)
	d_k = (cnn.hyper_parameters.max_conv_num - cur_conv_index)/cnn.hyper_parameters.max_conv_num * cur_sent_length;
	k = max([cnn.hyper_parameters.top_pooling_width d_k]);
end